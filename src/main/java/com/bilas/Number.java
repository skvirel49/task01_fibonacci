package com.bilas;

/**
 * Class manipulates numbers.
 * User adds interval to show odd and even numbers in interval.
 * Print sum of odd and even numbers.
 */
public class Number {

    private int start;
    private int finish;

    public void setStart(int start) {
        this.start = start;
    }

    public void setFinish(int finish) {
        this.finish = finish;
    }

    /**
     * method print odd numbers from start to finish.
     */
    public void printOddNumbersFromStart(){
        for (int i = start; i <= finish ; i++) {
            if (i % 2 == 0){
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    /**
     * method prints even numbers from finish to start
     */
    public void printEvenNumbersFromFinish(){
        for (int i = finish; i >= start; i--) {
            if (i % 2 != 0){
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    /**
     * method prints sum of odd numbers.
     * @return sum (sum of odd numbers).
     */
    public int printSumOfOddNumbers(){
        int sum = 0;
        for (int i = start; i <= finish ; i++) {
            if (i % 2 == 0){
                sum += i;
            }
        }
        System.out.println(sum);
        return sum;
    }

    /**
     * method prints sum of even numbers.
     * @return sum (sum of even numbers).
     */
    public int printSumOfEvenNumbers(){
        int sum = 0;
        for (int i = start; i <= finish ; i++) {
            if (i % 2 != 0){
                sum += i;
            }
        }
        System.out.println(sum);
        return sum;
    }

}
