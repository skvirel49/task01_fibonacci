package com.bilas;

import com.bilas.exceptions.MyException;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This class print odd and even numbers.
 * Build Fibonacci numbers and print biggest odd and even numbers.
 * Prints percentage of odd and even Fibonacci numbers.
 * @author Oleg Bilas
 * @version 1.1
 * @since 11.09.2019
 */

public class Main {

    public static Scanner scanner = new Scanner(System.in);

    /**
     * Method main starts program.
     * @param args
     * @throws MyException
     */
    public static void main(String[] args) throws MyException {
        fibonacciExecute();
        numberExecute();
    }

    /**
     * Method execute all methods from Fibonacci class
     */
    public static void fibonacciExecute(){
        Fibonacci fibonacci = new Fibonacci();
        System.out.println("add size of set:");

        try {
            int size = scanner.nextInt();
            fibonacci.fibonacciSet(size);
            fibonacci.printBiggestEvenFibonacci();
            fibonacci.printBiggestOddFibonacci();
            fibonacci.printPercentageOfEvenFibonacci();
            fibonacci.printPercentageOfOddFibonacci();
        } catch (InputMismatchException e){
            System.out.println("not number");
        }
    }

    /**
     * Method execute all methods from Number class.
     */
    public static void numberExecute(){
        Number number = new Number();

        System.out.println("add start num:");
        number.setStart(scanner.nextInt());
        System.out.println("add finish num:");
        number.setFinish(scanner.nextInt());

        number.printEvenNumbersFromFinish();
        number.printOddNumbersFromStart();
        number.printSumOfEvenNumbers();
        number.printSumOfOddNumbers();
    }

}