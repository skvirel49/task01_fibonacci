package com.bilas;

/**
 * Class manipulates Fibonacci numbers.
 * @author Oleg Bilas
 */
public class Fibonacci {
    private static final int PERCENTAGE = 100;
    private int oddNumCounter;
    private int evenNumCounter;

    private int maxOddNumber;
    private int maxEvenNumber;

    /**
     * Method shows Fibonacci set.
     * @param sizeOfSet
     */
    public void fibonacciSet(int sizeOfSet){

        int previousNumber = 0;
        int nextNumber = 1;

        System.out.print(previousNumber + " " + nextNumber);

        for (int i = 2; i < sizeOfSet; ++i) {
            /*
            On each iteration, we are assigning second number
            to the first number and assigning the sum of last two
            numbers to the second number
             */
            int sum = previousNumber + nextNumber;
            previousNumber = nextNumber;
            nextNumber = sum;

//            print Fibonacci number
            System.out.print(" " + sum);

//            count odd numbers and even numbers
            if (sum % 2 == 0) {
                oddNumCounter++;
                if (sum > maxOddNumber) {
                    maxOddNumber = sum;
                }
            } else {
                evenNumCounter++;
                if (sum > maxEvenNumber) {
                    maxEvenNumber = sum;
                }
            }

        }
        System.out.println();
    }

    /**
     * Method prints biggest odd number from Fibonacci set.
     */
    public void printBiggestOddFibonacci(){
        System.out.println("max odd num: " + maxOddNumber);
    }

    /**
     * Method prints biggest even number from Fibonacci set.
     */
    public void printBiggestEvenFibonacci(){
        System.out.println("max even num: " + maxEvenNumber);
    }

    /**
     * Method prints percentage of odd numbers in Fibonacci set
     */
    public void printPercentageOfOddFibonacci(){
        System.out.println("percentage of odds: " + ((double) oddNumCounter / (oddNumCounter + evenNumCounter) * PERCENTAGE));
    }

    /**
     * Method prints percentage of even numbers in Fibonacci set
     */
    public void printPercentageOfEvenFibonacci(){
        System.out.println("percentage of evens: " + ((double) evenNumCounter / (evenNumCounter + oddNumCounter) * PERCENTAGE));
    }
}
